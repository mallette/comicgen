# comicgen - Ceméa



## Préambule

Ce projet est une copie du projet (presque original) de "Gégé, le [générateur de Grisebouille](https://framalab.org/gknd-creator/)".
[Gee](https://ptilouk.net/), dessinateur, auteur, développeur et contributeur aux communs numériques, publie de manière régulière du contenu sous licence libre "Creative Commons By-Sa". Cette licence permet le rémploi et la modification de ce qu'il a produit.

En s'appuyant sur un autre projet de génération de BD ([Comic Gen de Willian Carvalho](https://github.com/willianpc/comicgen)), l'associaiton Framasoft a proposé une version adaptée en utilisant les dessins de Gee.

* [Voir les publications de Gee](https://grisebouille.net) : BD, romans, jeu vidéo, podcast... N'hésitez pas à [soutenir son travail](https://ptilouk.net/#soutien) par un don, achat, message de soutien...

## Description

Le projet hébérgé ici a vocation à proposer une version "améliorée" basée sur nos contributions et nos besoins d'évolutions au service de nos utilisateurs (usages sur smartphones...)

## License

Ce projet est diffusé sous licence [CC-by-SA](https://creativecommons.org/licenses/by-sa/4.0/).

## Evolutions souhaitées

* Amélioration de l'ergonomie générale
* Augmenter l'usabilité de l'application sur smartphone
* et ddes trucs auxquels on ne pense pas encore...