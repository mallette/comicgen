#!/usr/bin/env bash

URL="https://framalab.org/gknd-creator/toons/"


while IFS= read -r line
do
  echo "$line"
  fichier=${URL}${line}
  wget "$fichier"
done < toons-list.txt

